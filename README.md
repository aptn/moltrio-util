## Overview

A collection of command-line utilities for moltrio server administrators.

## Usage

### `moltrio syslogs`

Conveniently view the production logs of a client.

```
#!bash
moltrio syslogs CLIENT_NAME
```

### `moltrio update`

Run the update script on a client's moltrio

```
#!bash
moltrio update CLIENT_NAME
```

### `moltrio ssh`

Ssh's into an AWS instance (choosen ramdomly), for inspection or maintenance.

```
#!bash
moltrio ssh CLIENT_NAME # open a shell
moltrio ssh CLIENT_NAME COMMAND # execute COMMAND directly
```

### `moltrio pry`

Open a Pry console for a client.  The AWS instance it connects to is choosen
randomly.

```
#!bash
moltrio pry CLIENT_NAME # open a shell
```

### `moltrio dbconsole`

Just like `moltrio pry`, but instead opens the `dbconsole` of that client.

```
#!bash
moltrio dbconsole CLIENT_NAME
```

### `moltrio dump`

Dump the client's database to stdout.

```
#!bash
moltrio dump daily CLIENT_NAME
```

### `moltrio setup`

This one must be ran in a Moltrio root.  It has two subcommands, `create` and
`show`.  Run `moltrio setup -h` for more information.  In summary, it takes a
database dump from stdin and loads it to the database described in
`config/database.yml`, according to the `RAILS_ENV`.

Best used in conjunction with `moltrio dump`.  Example:

```
#!bash
moltrio dump daily CLIENT_NAME | moltrio setup create
```

## Installation

To install, copy and paste this on your terminal (assuming you use bash):

```
#!bash
cd ~
git clone git@bitbucket.org:aptn/moltrio-util .moltrio-util

# Ensure ~/.local/bin exists and is in the PATH
[[ -d ~/.local/bin ]] || mkdir ~/.local/bin
if [[ ":$PATH:" != *":$HOME/.local/bin:"* ]]; then
  export PATH="$HOME/.local/bin:$PATH"
  echo !!:q >> ~/.bashrc
fi

ln -s ~/.moltrio-util/bin/moltrio ~/.local/bin/
```

To get command-line completion add to your `~/.bashrc`:

```
#!bash
source ~/.moltrio-util/script/completion.sh
```

Or, if you use fish (adjust to your `$fish_complete_paths` setting):

```
#!bash
mkdir -p ~/.config/fish/completions
ln -s ~/.moltrio-util/script/moltrio.fish ~/.config/fish/completions/
```

To update:

```
#!bash
cd ~/.moltrio-util
git pull origin master
```

**Note**: the `~/.moltrio-util` path is a suggestion only, and is not
hardcoded in the source. You may change it at will.
