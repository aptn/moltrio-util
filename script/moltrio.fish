function __moltrio_utils_home
    fish -c "cd (dirname (readlink -e (which moltrio)))/..; and pwd -P"
end

function __moltrio_command_list
    set bin_path (__moltrio_utils_home)/bin

    for cmd in "$bin_path"/moltrio-*
        test -x $cmd; and echo (basename $cmd | cut -d- -f2-)
    end
end

function __moltrio_completed_words
    set words (echo $argv[1] | wc -w)
    if begin; echo $argv[1] | grep --color=never -v -P "\s+\$" > /dev/null; end
        set words (math $words - 1)
    end
    echo $words
end

function __moltrio_current_command_valid
    set blacklisted_commands list ssh host-ssh rsync scp
    set cmd (echo $argv[1] | cut -d' ' -f2)
    test -z $cmd; and return 1
    for blacklisted in $blacklisted_commands
        test $blacklisted = $cmd; and return 1
    end
    return 0
end

set -l commands "test (__moltrio_completed_words (commandline -c)) -eq 1"
set -l containers "test (__moltrio_completed_words (commandline -c)) -eq 2; and __moltrio_current_command_valid (commandline -c)"

complete -c moltrio -n $commands -A -f -a "(__moltrio_command_list)"
complete -c moltrio -n $containers -A -f -a "(moltrio list)"
