__moltrio_command_list() {
  local moltrio_utils_home=$(
    cd $(dirname ${BASH_SOURCE[0]})/..
    pwd -P
  )

  local bin_path="$moltrio_utils_home/bin"

  for comm in "$bin_path"/moltrio-*; do
    [[ -x "$comm" ]] && echo "${comm##$bin_path/moltrio-}"
  done
}

__moltrio_complete() {
  local word_count="${#COMP_WORDS[@]}"
  local comm="${COMP_WORDS[1]}"

  if [[ $word_count -eq 2 ]]; then
    COMPREPLY=( $(compgen -W "$(__moltrio_command_list)" "${COMP_WORDS[-1]}") )
  elif [[ $word_count -eq 3 && "$comm" != "list" && "$comm" != "ssh" && "$comm" != "host-ssh" && "$comm" != "rsync" && "$comm" != "scp"]]; then
    COMPREPLY=( $(compgen -W "$(moltrio list)" "${COMP_WORDS[-1]}") )
  fi
}

complete -F __moltrio_complete moltrio
